package com.flight.api.cache;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

@Component
public class CacheManager {

    private final static Map<String, Cacheable> cacheHashMap = new HashMap<>();
    private final static int MILLIS_TO_SLEEP = 5000;

    @PostConstruct
    public void init(){
        try {
            Thread threadCleanerUpper = new Thread(
                    () -> {
                        try {
                            while (true) {
                                synchronized (cacheHashMap) {
                                    Set keySet = cacheHashMap.keySet();
                                    Iterator keys = keySet.iterator();

                                    while (keys.hasNext()) {
                                        Object key = keys.next();
                                        Cacheable value = cacheHashMap.get(key);
                                        if (value.isExpired()) {
                                            keys.remove();
                                        }
                                    }
                                }
                                Thread.sleep(MILLIS_TO_SLEEP);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });

            threadCleanerUpper.start();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }


    public static Cacheable getCache(String identifier) {
        synchronized (cacheHashMap) {
            Cacheable object = cacheHashMap.get(identifier);
            if (object == null)
                return null;
            if (object.isExpired()) {
                cacheHashMap.remove(identifier);
                return null;
            } else {
                return object;
            }
        }
    }

    public static void putCache(Cacheable object) {
        synchronized (cacheHashMap) {
            cacheHashMap.put(object.getIdentifier(), object);
        }
    }
}
