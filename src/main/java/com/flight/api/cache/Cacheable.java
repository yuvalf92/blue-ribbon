package com.flight.api.cache;

public interface Cacheable {

    boolean isExpired();

    String getIdentifier();

    Object getObject();
}
