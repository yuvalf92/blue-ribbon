package com.flight.api.cache;

import java.util.Calendar;
import java.util.Date;

public class CachedObject implements Cacheable {

    private Date expirationDate;
    private String indentifier;
    private Object object;

    public CachedObject(Object obj, String id, int ttl) {
        this.object = obj;
        this.indentifier = id;
        if (ttl != 0) {
            this.expirationDate = new Date();
            Calendar calendar = java.util.Calendar.getInstance();
            calendar.setTime(expirationDate);
            calendar.add(Calendar.SECOND, ttl);
            expirationDate = calendar.getTime();
        }
    }

    public boolean isExpired() {
        return expirationDate != null && expirationDate.before(new Date());
    }

    public String getIdentifier() {
        return this.indentifier;
    }

    public Object getObject(){
        return this.object;
    }
}
