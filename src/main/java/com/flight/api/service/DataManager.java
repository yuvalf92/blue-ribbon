package com.flight.api.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flight.api.dto.Coupon;
import com.flight.api.dto.Data;
import com.flight.api.dto.Flight;
import com.flight.api.dto.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataManager {

    private static final Logger LOG = LoggerFactory.getLogger(DataManager.class);
    private ObjectMapper objectMapper = new ObjectMapper();
    private Data data;
    private List<Ticket> tickets;

    @PostConstruct
    public void init() {


        LOG.info("reading data..");
        InputStream inputStream = TypeReference.class.getResourceAsStream("/flights.json");
        try {
            data = objectMapper.readValue(inputStream, Data.class);

           tickets = data.getFlights().stream()
                    .map(Flight::getTickets)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            LOG.error("failed to parse data!", e);
        }
    }

    public List<Flight> getFlights() {
        return data.getFlights();
    }

    public List<Coupon> getCoupons() {
        return data.getCoupons();
    }

    public List<Ticket> getTickets(){
        return tickets;
    }
}
