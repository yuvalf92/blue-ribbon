package com.flight.api.service;

import com.flight.api.dto.CheckinRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class RequestValidator {

    public void validateCheckinRequest(CheckinRequest request){
        if(StringUtils.isEmpty(request.getDestinationId()) || StringUtils.isEmpty(request.getBaggageId())){
            throw new IllegalArgumentException("destinationId and baggageId must be provided");
        }
    }
}
