package com.flight.api.service;

import com.flight.api.cache.CacheManager;
import com.flight.api.cache.Cacheable;
import com.flight.api.cache.CachedObject;
import com.flight.api.dto.CheckinRequest;
import com.flight.api.dto.CheckinResponse;
import com.flight.api.dto.Coupon;
import com.flight.api.dto.CouponResponse;
import com.flight.api.dto.Flight;
import com.flight.api.dto.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import sun.misc.Cache;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FlightService {

    private final static Logger LOG = LoggerFactory.getLogger(FlightService.class);
    private final static int TTL = 100;

    @Autowired
    private DataManager dataManager;

    public boolean isTicketAvailable(int ticketId) {
        final String cacheId = String.format("Ticket%d", ticketId);

        Cacheable fromCache = CacheManager.getCache(cacheId);
        if (fromCache != null && fromCache.getObject() != null) {
            LOG.info("retrieving answer from cache");
            return (boolean) fromCache.getObject();
        }

        Optional<Ticket> optionalTicket = dataManager.getTickets().stream()
                .filter(t -> t.getTicketId() == ticketId)
                .findFirst();

        boolean isAvailable = optionalTicket.map(Ticket::isAvailable).orElse(false);

        CacheManager.putCache(new CachedObject(isAvailable, cacheId, TTL));
        return optionalTicket.map(Ticket::isAvailable).orElse(false);
    }

    public CouponResponse useCoupon(int couponId, double price) {

        List<Coupon> coupons = dataManager.getCoupons();
        Optional<Coupon> coupon = coupons.stream()
                .filter(c -> c.getCouponId() == couponId)
                .findFirst();

        if (!coupon.isPresent()) {
            LOG.info("coupon {} wan't found", couponId);
            return new CouponResponse(false, price);
        }

        double discount = coupon.get().getDiscount();
        double finalPrice = getFinalPrice(price, discount);

        return new CouponResponse(true, finalPrice);

    }

    public CheckinResponse checkin(CheckinRequest checkinRequest) {
        List<Flight> flightsToDest = dataManager.getFlights().stream()
                .filter(f -> f.getDestinationId() == (checkinRequest.getDestinationId()))
                .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(flightsToDest)) {
            LOG.info("no flights to {} were found", checkinRequest.getDestinationId());
            return new CheckinResponse(false);
        }

        Flight flight = flightsToDest.get(0);

        CheckinResponse checkinResponse = new CheckinResponse();
        checkinResponse.setFlightId(flight.getFlightId());
        checkinResponse.setBaggageId(checkinRequest.getBaggageId());
        checkinResponse.setCheckinSucceeded(flight.isBaggageAllowed());

        return checkinResponse;
    }


    private double getFinalPrice(double price, double discount) {
        return discount == 0 ? price : price * (1 - (discount / 100));
    }
}
