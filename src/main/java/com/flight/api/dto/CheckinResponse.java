package com.flight.api.dto;

public class CheckinResponse {

    private boolean checkinSucceeded;
    private String baggageId;
    private int flightId;

    public CheckinResponse() {
    }

    public CheckinResponse(boolean checkinSucceeded) {
        this.checkinSucceeded = checkinSucceeded;
    }

    public boolean isCheckinSucceeded() {
        return checkinSucceeded;
    }

    public void setCheckinSucceeded(boolean checkinSucceeded) {
        this.checkinSucceeded = checkinSucceeded;
    }

    public String getBaggageId() {
        return baggageId;
    }

    public void setBaggageId(String baggageId) {
        this.baggageId = baggageId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }
}
