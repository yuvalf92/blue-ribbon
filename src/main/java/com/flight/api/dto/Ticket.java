package com.flight.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Ticket {

    @JsonProperty("ticketId")
    private int ticketId;

    @JsonProperty("available")
    private boolean isAvailable;

    @JsonProperty("price")
    private double price;


    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
