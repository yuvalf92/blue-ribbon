package com.flight.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Flight {

    @JsonProperty("flightId")
    private int flightId;

    @JsonProperty("origin")
    private int originId;

    @JsonProperty("destination")
    private int destinationId;

    @JsonProperty("tickets")
    private List<Ticket> tickets;

    @JsonProperty("baggageAllowed")
    private boolean baggageAllowed;


    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public int getOriginId() {
        return originId;
    }

    public void setOriginId(int originId) {
        this.originId = originId;
    }

    public int getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(int destinationId) {
        this.destinationId = destinationId;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public boolean isBaggageAllowed() {
        return baggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        this.baggageAllowed = baggageAllowed;
    }
}
