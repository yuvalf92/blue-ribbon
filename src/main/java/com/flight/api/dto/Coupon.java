package com.flight.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Coupon {

    @JsonProperty("couponId")
    private int couponId;

    @JsonProperty("discount")
    private double discount;

    @JsonProperty("flightId")
    private int flightId;

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }
}
