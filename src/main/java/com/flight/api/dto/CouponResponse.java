package com.flight.api.dto;

public class CouponResponse {

    private boolean isCouponValid;
    private double flightPrice;

    public CouponResponse(boolean isCouponValid, double flightPrice) {
        this.isCouponValid = isCouponValid;
        this.flightPrice = flightPrice;
    }

    public boolean isCouponValid() {
        return isCouponValid;
    }

    public void setCouponValid(boolean couponValid) {
        isCouponValid = couponValid;
    }

    public double getFlightPrice() {
        return flightPrice;
    }

    public void setFlightPrice(double flightPrice) {
        this.flightPrice = flightPrice;
    }
}
