package com.flight.api.controller;

import com.flight.api.dto.CheckinRequest;
import com.flight.api.dto.CheckinResponse;
import com.flight.api.dto.CouponResponse;
import com.flight.api.service.FlightService;
import com.flight.api.service.RequestValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlightController {

    private final static Logger LOG = LoggerFactory.getLogger(FlightController.class);

    @Autowired
    private FlightService flightService;

    @Autowired
    private RequestValidator validator;

    @GetMapping("/ticket")
    public ResponseEntity<Boolean> isTicketAvailable(@RequestParam(name = "ticketId") Integer ticketId) {

        boolean isAvailable = flightService.isTicketAvailable(ticketId);
        return new ResponseEntity<>(isAvailable, HttpStatus.OK);
    }

    @PostMapping("/checkin/")
    public ResponseEntity<CheckinResponse> checkIn(@RequestBody CheckinRequest checkinRequest) {

        validator.validateCheckinRequest(checkinRequest);
        CheckinResponse response = flightService.checkin(checkinRequest);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/coupon")
    public ResponseEntity<CouponResponse> useCoupon(@RequestParam(name = "couponId") Integer couponId,
                                                    @RequestParam(name = "price") Double price) {

        CouponResponse response = flightService.useCoupon(couponId, price);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException ex) {
        LOG.error("Error: {}", ex);
        return new ResponseEntity<>(String.format("Input is not valid. %s", ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<String> handleGenericException(Exception ex) {
        LOG.error("Error: {}", ex);
        return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
